using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    public float vitesse = 5f;
    public float vitesseRotation = 100f;
    public float forceSaut = 5f;
    public bool estAuSol;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
        transform.Translate(Vector3.forward * vitesse * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
        transform.Translate(Vector3.back * vitesse * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
        transform.Rotate(Vector3.up, -vitesseRotation * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
        transform.Rotate(Vector3.up, vitesseRotation * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.back * vitesse * Time.deltaTime);
        }
        if (Input.GetKeyDown(KeyCode.Space) && estAuSol)
        {
            rb.AddForce(Vector3.up * forceSaut, ForceMode.Impulse);
            estAuSol = false;
        }
       
    }
    // function pour savoir si le joueur touche au sol
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            estAuSol = true;
        }
    }
}
