using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class poursuite : MonoBehaviour
{
    public Transform joueur; // Le joueur � poursuivre
    public float vitesse = 2f; // Vitesse de l'adversaire
    public float distancePoursuite = 10f; // Distance � laquelle l'adversaire commence � poursuivre

    void Update()
    {
        // Calcul de la distance entre l'adversaire et le joueur
        float distance = Vector3.Distance(transform.position, joueur.position);

        // Si la distance est inf�rieure � la distance de poursuite, l'adversaire poursuit le joueur
        if (distance < distancePoursuite)
        {
            // L'adversaire regarde en direction du joueur
            transform.LookAt(joueur);

            // L'adversaire avance vers le joueur
            transform.Translate(Vector3.forward * vitesse * Time.deltaTime);
        }
    }
}
